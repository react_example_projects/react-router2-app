import PageContent from '../PageContent';

export default function HomePage() {
  return (
    <PageContent title="Welcome!">
      <p>Browse all our amazing event!</p>
    </PageContent>
  );
}
